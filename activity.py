year = int(input("Please input a year: \n"))
if year <= 0:
    print("Year must be a positive integer")
else: 
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        print(f"{year} is a leap year.")
    else:
        print(f"{year} is not a leap year.")

rows = int(input("Enter number of rows: \n"))
cols = int(input("Enter number of cols: \n"))
if rows <= 0 or cols <= 0:
    print("Both rows and columns must be positive integers")
r = 1
c = 1
output = ""
while r <= rows:
    while c <= cols:
        output += "*"
        c += 1
    print(output)
    r += 1


